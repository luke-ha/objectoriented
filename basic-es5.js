function Users(fName, lName) {
    if(this instanceof Users) {
        this.firstName = fName;
        this.lastName = lName;
    } else {
        return new Users(fName, lName);
    }
}

var user1 = Users('Steve', 'Smith');
