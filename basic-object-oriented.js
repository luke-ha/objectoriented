

// Factory function
// function createCircle(radius) {
//     return {
//         radius,
//         draw: function() { // we call draw is a method 
//             console.log('drawing');
//         }
//     }
// }

// const circle = createCircle(1);


// let obj = {value: 10};

// function increase(obj) {
//     obj.value++;
// }

// increase(obj);

// console.log(obj);

// Constructor function
function Circle(radius) {
    this.radius = radius;
    let defaultLocation = {x: 0, y: 0}

    this.getDefaultLocation = function() {
        return defaultLocation;
    }
    
    this.draw = function() {
        console.log('Drawing');
    };

    Object.defineProperty(this, 'defaultLocation', {
            get: function() {
                return defaultLocation;
            },
            set: function(value){
                if(!value.x || !value.y) {
                    throw new Error('Invalid location');
                }
                defaultLocation = value;
            }
        }
    )
} 

// Literal Object

let litObject = {
    name: 'Bernard',
    age: 28,
    get getName() {
        return this.name;
    },
    set setName(value) {
        this.name = value
    }
}
// litObject.setName = 123
// console.log(litObject.getName);

// const circle = new Circle(10)
// circle.defaultLocation = {x: 123, y: 12322};
// console.log(circle.defaultLocation);

function StopWatch () {
    let status = 'initial',
    startTime = 0,
    endTime = 0;

    this.start = function () {
        if(status === 'started') {
            throw new Error('Watch was started');
        }
        status = 'started';
        startTime = (new Date).getTime();
        endTime = 0;
    }

    this.stop = function(){
        if(status === 'stopped') {
            throw new Error('Watch was stopped');
        }
        status = 'stopped';
        endTime = (new Date).getTime();
    }
    this.reset = function(){
        endTime = 0;
        startTime = 0;
    }

    Object.defineProperty(this, 'duration', {
        get: function(){
            console.log(endTime - startTime);
        }
    })
}


